import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Map {

    private static final String RESET = "\u001B[0m";

    private static final String WHITE = "\u001B[30m";

    private static final String RED = "\u001B[31m";

    private static final String GREEN = "\u001B[32m";

    private static final String YELLOW = "\u001B[33m";

    private static final String BLUE = "\u001B[34m";

    private static final String PURPLE = "\u001B[35m";

    private static final String CYAN = "\u001B[36m";

    private final Set<Human> humans = new HashSet<>();

    private final int mapX, mapY;

    public Map(int x, int y) {
        this.mapX = x;
        this.mapY = y;
    }

    public void remove(Human character, Map map) {
        for (int i = 0; i < humans.size(); i++) {
            if (character == findHuman(character.getX(), character.getY())) {
                humans.remove(character);
            }
        }
    }

    public int getMaxX() {
        return mapX;
    }

    public int getMaxY() {
        return mapY;
    }

    public Human findHuman(int posX, int posY) {
        for (Human human : humans) {
            if (posX == human.getX() && posY == human.getY()) {
                return human;
            }
        }
        return null;
    }

    public void spawn(Human character) {
        humans.add(character);
    }


    public void showEntities() {
        for (int posY = 0; posY < mapY; posY++) {
            StringBuilder s = new StringBuilder();
            for (int posX = 0; posX < mapX; posX++) {
                s.append("[");
                Human human = findHuman(posX, posY);
                if (human instanceof Priest) {
                    s.append(PURPLE + "P" + RESET);
                } else if (human instanceof Rambo) {
                    s.append(RED + "R" + RESET);
                } else if (human instanceof Rapper && !((Rapper) human).getCurse()) {
                    s.append(YELLOW + "r" + RESET);
                } else if (human instanceof Rapper && ((Rapper) human).getCurse()) {
                    s.append(YELLOW + "(r)" + RESET);
                } else if (human instanceof Scientist) {
                    s.append(CYAN + "S" + RESET);
                } else if (human instanceof UmpaLumpa) {
                    s.append(BLUE + "U" + RESET);
                } else if (human != null) {
                    s.append("H");
                } else {
                    s.append(" ");
                }
                s.append("]");
            }
            System.out.println(s);
        }
        System.out.println();
    }

    public void generateHumans(ArrayList<Human> humans, int count) {
        int x = (int) (Math.random() * (mapX - 1));
        int y = (int) (Math.random() * (mapY - 1));

        for (int i = 0; i < count; i++) {
            if (isEmpty(x, y)) {
                humans.add(new Human(x, y));
                spawn(humans.get(i));
            } else {
                x = (int) (Math.random() * (mapX - 1));
                y = (int) (Math.random() * (mapY - 1));
                humans.add(new Human(x, y));
            }
        }
    }

    public void generateUmpas(ArrayList<Human> umpa, int count) {
        int x = (int) (Math.random() * (mapX - 1));
        int y = (int) (Math.random() * (mapY - 1));

        for (int i = 0; i < count; i++) {
            if (isEmpty(x, y)) {
                umpa.add(new UmpaLumpa(x, y));
                spawn(umpa.get(i));
            } else {
                x = (int) (Math.random() * (mapX - 1));
                y = (int) (Math.random() * (mapY - 1));
                umpa.add(new UmpaLumpa(x, y));
                spawn(umpa.get(i));
            }
        }
    }

    public void moveNPCs(ArrayList<Human> humans, Map map) {
        for (int i = 0; i < humans.size(); i++) {
            int location = (int) Math.random() * 5;

            if (location == 0) {
                location++;
            }

            switch (location) {
                case 1:
                    humans.get(i).moveTo("up", map);
                    break;
                case 2:
                    humans.get(i).moveTo("down", map);
                    break;
                case 3:
                    humans.get(i).moveTo("left", map);
                    break;
                case 4:
                    humans.get(i).moveTo("right", map);
                    break;
            }
        }
    }

    public boolean isEmpty(int x, int y) {
        boolean flag = false;
        if (findHuman(x, y) == null) {
            flag = true;
        }
        return flag;
    }
}
