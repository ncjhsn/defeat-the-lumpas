import jdk.swing.interop.SwingInterOpUtils;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        Boolean end = false;
        int selectedPlayer = 0;
        String location;
        int answer = 0;
        int rounds = 1;
        int answerAttack = 0;
        int action = 0;
        int sort = 0;
        int playersAlive = 4;
        int roundsCursed = 0;
        int graves = 0;
        Map god = new Map(16, 16);
        Scientist scientist = new Scientist(3, 0);
        Rambo rambo = new Rambo(1, 0);
        Priest priest = new Priest(2, 0);
        Rapper rapper = new Rapper(0, 0);
        ArrayList<Human> players = new ArrayList<Human>();

        ArrayList<Human> umpas = new ArrayList<>();
        ArrayList<Human> humans = new ArrayList<>();

        players.add(rambo);
        players.add(rapper);
        players.add(scientist);

        god.generateHumans(humans, 18);
        god.generateUmpas(umpas, 18);
        god.spawn(priest);
        god.spawn(rambo);
        god.spawn(scientist);
        god.spawn(rapper);
        god.showEntities();

        while (!end) {
            if (rapper.getCurse()) {
                rapper.randomMove(god);
                for (int i = 0; i < players.size(); i++){
                    rapper.attack(god, players.get(i));

                }
                for (int i = 0; i < umpas.size(); i++){
                    rapper.attack(god, umpas.get(i));
                }
                for (int i = 0; i < humans.size(); i++){
                    rapper.attack(god, humans.get(i));
                }
                rapper.decreaseCurse();
                if (rapper.getRoundsCursed() == 0) {
                    rapper.swap();
                }
            }
            System.out.println("WANT TO SELECT SOMEONE?");
            System.out.println("[1] - YES / [0] - NO");
            answer = s.nextInt();
            s.nextLine();
            if (answer == 0) {
                System.out.println(":^)");
                god.moveNPCs(humans, god);
                god.moveNPCs(umpas, god);

            } else if (answer == 1) {
                System.out.println("WHO? ");
                System.out.println("[1] - PRIEST     [3] - RAMBO \n[2] - SCIENTIST  [4] - RAPPER");
                selectedPlayer = s.nextInt();
                s.nextLine();
                System.out.println("[ATTACK OR MOVE] \n [1] - ATTACK [2] - MOVE [3] - CANCEL");
                action = s.nextInt();
                s.nextLine();
                if (action == 1) {
                    if (selectedPlayer == 1) {
                        System.out.println("DEU BRET CUPINSCHER, TU NAO PODE ATACAR COM O PRIEST, PERDEU A JOGADA");

                    } else if (selectedPlayer == 2) {
                        int rand = scientist.sort();
                        if (scientist.getPoison() > 0) {
                            for (int i = 0; i < umpas.size(); i++) {
                                scientist.attack(god, umpas.get(i));
                                scientist.poisonDec();

                            }
                            graves = 0;
                        }
                        if (rand <= 60 && scientist.getPoison() == 0) {
                            System.out.println(rand);
                            for (int i = 0; i < umpas.size(); i++) {
                                scientist.attack(god, umpas.get(i));
                            }
                            graves = 0;
                        } else {
                            sort = (int) Math.random() * 11;
                            if (sort <= 7) {
                                god.remove(scientist, god);
                                playersAlive--;
                            }
                            graves = 0;
                        }

                    } else if (selectedPlayer == 3) {
                        if (rambo.sort() >= 90) {
                            for (int i = 0; i < umpas.size(); i++) {
                                rambo.attack(god, umpas.get(i));
                            }
                            graves = 0;
                        } else {
                            rambo.hitAdd();
                            if (rambo.getHits() == 5) {
                                god.remove(rambo, god);
                                playersAlive--;
                            }
                            graves = 0;
                        }

                    } else if (selectedPlayer == 4 && !rapper.getCurse()) {
                        if (rapper.sort() <= 65) {
                            for (int i = 0; i < umpas.size(); i++) {
                                rapper.attack(god, umpas.get(i));
                            }
                        }
                        graves = 0;
                    } else {
                        System.out.println("RAPPER ISNT FEELING OK");
                    }
                } else if (action == 2) {
                    System.out.print("WHERE? \n[UP] [LEFT] [DOWN] [RIGHT]");

                    if (selectedPlayer == 1) {
                        System.out.print("MOVE PRIEST TO: ");
                        location = s.nextLine();
                        priest.moveTo(location, god);

                    } else if (selectedPlayer == 2) {
                        System.out.print("MOVE SCIENTIST TO: ");
                        location = s.nextLine();
                        scientist.moveTo(location, god);
                        if (scientist.sort() < 8) {
                            scientist.poisonAdd();
                        }
                    } else if (selectedPlayer == 3) {
                        System.out.println("MOVE RAMBO TO: ");
                        location = s.nextLine();
                        rambo.moveTo(location, god);

                    } else if (selectedPlayer == 4 && !rapper.getCurse()) {
                        System.out.println("MOVE RAPPER TO: ");
                        int rand = rapper.sort();
                        location = s.nextLine();
                        rapper.moveTo(location, god);
                        if (rand < 8 && !rapper.getCurse()) {
                            rapper.swap();
                        }
                    }

                } else {
                    System.out.println("?!?!?!");
                }
            }


            god.moveNPCs(humans, god);
            god.moveNPCs(umpas, god);
            god.showEntities();
            graves++;
            if (playersAlive == 1 || umpas.size() == 0 || humans.size() == 0 || graves == 5) {
                end = true;
            }
            rounds++;
        }


    }

}
