public class Scientist extends Human{

    private int poison;

    public Scientist(int x, int y) {
        super(x,y);
        this.poison = 0;
    }

    public void poisonDec(){
        poison -= 1;
    }

    public int getPoison(){
        return poison;
    }

    public void poisonAdd(){
        poison += 5;
    }

}