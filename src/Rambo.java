public class Rambo extends Human{

    private double damage;
    private int hits;

    public Rambo(int x, int y){
        super(x,y);
        hits = 0;
    }

    public void hitAdd(){
        hits += 1;
    }

    public void hitDec(){
        hits -= 1;
    }

    public int getHits(){
        return hits;
    }

    public double attack(){
        damage= (Math.random()*100+1);
        return damage;
    }
}