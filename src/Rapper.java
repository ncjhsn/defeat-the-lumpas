public class Rapper extends Human {

    private boolean curse;
    private int roundsCursed;

    public Rapper(int x, int y) {
        super(x, y);
        curse = false;
    }

    public boolean getCurse() {
        return curse;
    }

    public void swap() {
        curse = !curse;
        if (curse) {
            roundsCursed = 5;
        }
    }

    public int getRoundsCursed() {
        return roundsCursed;
    }

    public void decreaseCurse() {
        roundsCursed--;
    }

    public void randomMove(Map map) {

        int location = (int) (Math.random() * 5);
        if (location == 0) {
            location++;
        }
        switch (location) {
            case 1:
                moveTo("up", map);
                break;
            case 2:
                moveTo("down", map);
                break;
            case 3:
                moveTo("left", map);
                break;
            case 4:
                moveTo("right", map);
                break;
        }
    }
}
